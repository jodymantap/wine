import {combineReducers} from 'redux';

const initialState = {
  cartTotal: 0,
  isLogin: false,
  data: [],
  isLoading: false,
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case 'getData': {
      return {
        ...state,
        isLoading: true,
      };
    }
    case 'getDataSuccess': {
      return {
        ...state,
        isLoading: false,
        data: state.data.concat(action.payload),
      };
    }
    case 'getDataFailed': {
      return {
        ...state,
        isLoading: false,
      };
    }
    case 'addtocart':
      return {
        ...state,
        cartTotal: state.cartTotal + 1,
      };
    default:
      return state;
  }
};

export default combineReducers({auth});
