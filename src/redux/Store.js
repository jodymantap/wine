import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleWare from 'redux-saga';
import Reducer from './Reducer';
import rootSaga from './Sagas';

const sagaMiddleWare = createSagaMiddleWare();
const store = createStore(Reducer, applyMiddleware(sagaMiddleWare));
sagaMiddleWare.run(rootSaga);
export default store;