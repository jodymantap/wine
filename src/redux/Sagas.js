import {all, put, takeLatest} from "redux-saga/effects";
import {apiGetData} from './Api';

function* getData(action) {
    try{
        const res = yield apiGetData(action.payload);
        if (res && res.data) {
            // console.log(res.data.value.products);
            yield put({type: "getDataSuccess", payload: res.data.value.products})
        } else yield put({type: "getDataFailed"})
    } catch(e){
        yield put({type: "getDataFailed"})
    }
}

function* fetchData() {
    yield takeLatest("getData", getData);
}

export default function* rootSaga() {
    yield all ([fetchData()]);
}