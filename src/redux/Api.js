import axios from 'axios';

const baseUrl = `https://zax5j10412.execute-api.ap-southeast-1.amazonaws.com/dev/api/product/list?page=`;

export const apiGetData = (data) => {
    return axios({
        method: "GET",
        url: baseUrl+data
    })
}